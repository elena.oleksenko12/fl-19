const fs = require('fs');
const path = require('path');
const pathFiles = path.join(__dirname, 'files');
const extensionsArr = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

function createFile(req, res, next) {
  try {
const filename = req.body.filename;
const content = req.body.content;
const filePath = path.join(__dirname, 'files', `${filename}`);
const fileExtension = path.extname(filename).split('.')[1];

if (content === undefined) {
  res.status(400).json({message: 'Please specify "content" parameter'});
  return;
}

if (extensionsArr.includes(fileExtension)) {
  fs.writeFileSync(filePath, content);
  res.status(200).send({'message': 'File created successfully'});
} else {
  res.status(400).json({
    message: 'Invalid extension'});
}
  } catch(err) {
     res.status(400).json({message: 'Server error'});
  } 
}

function getFiles (req, res, next) {
  try {
  let files = fs.readdirSync(path.join(pathFiles));
  
    res.status(200).send(
      {
        "message": "Success",
        "files":  files
      });
  } catch(err) {
    res.status(400).send({'message': 'Client error'});
  }
}

const getFile = (req, res, next) => {
  try {
    const FileName = req.params.filename;
    let adress = path.join(__dirname,'files',FileName);
    const pathExtention = path.extname(adress).slice(1);

    if (!fs.existsSync(adress)) {
      return res.status(400).json({'message': `No file with '${FileName}' filename found`});
    }
    let uploadedDate = fs.statSync(adress).mtime;
   
    console.log(uploadedDate);
    let fileContent = fs.readFileSync(adress, { encoding: 'utf-8' });
    return res.status(200).send(
               {
                 "message": "Success",
                 "filename": FileName,
                 "content": fileContent,
                 "extension": pathExtention,
                 "uploadedDate": uploadedDate
               }
               );
  } catch(err) {
    res.status(400).json({message: 'Server error'});
  }
}
   
const editFile = (req, res, next) => {
  try {
    const FileNameId = req.params.filename;
    let adress = path.join(__dirname,'files',FileNameId);
    let content = req.body.content;

    if (!fs.existsSync(adress)) {
      return res.status(400).send({'message': `File not Found`});
    }
  
    fs.writeFileSync(adress, content);
    return res.status(200).json({ 'message': 'File edited successfully' });
  } catch(err) {
    res.status(500).json({message: 'Server error'});
  }
}

const deleteFile = (req, res, next) => {
  try {
    const FileNameId = req.params.filename;
    let adress = path.join(__dirname,'files',FileNameId);

    if (!fs.existsSync(adress)) {
      return res.status(400).send({'message': `File not Found`});
    }
    fs.unlinkSync(adress, (error) => {   
        error ? console.log(error) : null});
        res.status(200).send({'message': 'File deleted successfully'}); 
  } catch(err) {
    res.status(500).json({message: 'Server error'});
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile
}
